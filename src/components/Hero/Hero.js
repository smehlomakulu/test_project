import React from 'react';
import classes from './Hero.module.scss';
import bg from '../../assets/images/bg.jpg';

const Hero = () => {
    return (
        <div className={classes.Hero} style={{ background: `#fff url(${bg}) no-repeat center bottom` }}>
            <nav>
                <ul>
                    <li><a href="!#">Home</a></li>
                    <li><a href="!#">Portfolio</a></li>
                    <li><a href="!#">Clients</a></li>
                    <li><a href="!#">Technologies</a></li>
                    <li><a href="!#">Contact</a></li>
                </ul>
            </nav>

            <div className={classes.Statement}>
                <h1><span>Tranquility</span> resides here</h1>
                <p>This is the best place you can find for <span>accommodation</span> in this region, <br />5 Start dining amenities</p>
            </div>
        </div>
    )
}

export default Hero
