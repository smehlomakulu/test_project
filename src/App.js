import React from 'react';
import Hero from './components/Hero/Hero';
import classes from './App.module.scss';

function App() {
  return (
    <div className={classes.App}>
      <Hero />
    </div>
  );
}

export default App;
